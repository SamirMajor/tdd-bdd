# Calculatrice

## Récupération du projet

Une fois tous les fichiers récupérés,ouvrez le projet et placez-vous à la racine du projet:

- cd tdd-bdd

Ouvrez le terminal et lancer les commandes suivante afin de pouvoir lancer le projet:

- npm install
- npm install prompt-sync
- npm install chai
- npm install mocha

## Utilisation de la Calculatrice

Déplacez vous dans le dossier src :

- cd src

et lancez le fichier calculator.js avec la commande suivante:

- start calculator.js

Maintenant que le fichier debug, il vous faut pour utiliser la Calculatrice procéder de la manière suivante :

    Tapez le premier chiffre que vous souhaitez, puis appuyez sur la touche "Entrée"
    
    Tapez l'opérateur que vous souhaitez utiliser
    
    Réitérez la première étape

Une fois ces 3 étapes réalisées, le résultat s'affiche !

## Les Test

Déplacez vous dans le dossier "test" :

- cd test

et tapez la commande suivante afin d'effectuer les test Bdd et Tdd :

- npm test

# Plan de test BDD et TDD

## Étape de préparation :

Définir les cas de test à exécuter
Déterminer les résultats attendus pour chaque cas de test
Préparer l'environnement de test (par exemple, configurer l'outil de test et les données de test)

## Étape de test BDD :

Rédiger une description du comportement attendu de la calculatrice en utilisant une syntaxe spécifique 
Exécuter les cas de test et vérifier que le comportement de la calculatrice correspond à ce qui est décrit dans la description

## Étape de test TDD :

Écrire un premier cas de test pour la calculatrice avec mocha et chai
Exécuter le cas de test et vérifier qu'il échoue (car la fonctionnalité n'a pas encore été implémentée)
Implémenter la fonctionnalité de la calculatrice en suivant les spécifications du cas de test
Exécuter à nouveau le cas de test et vérifier qu'il réussit
Répéter ce processus pour chaque cas de test supplémentaire
Il est important de noter que le processus de test BDD et TDD peut être itératif et que les étapes peuvent être répétées plusieurs fois au cours du développement de la calculatrice. De plus, il est recommandé de couvrir un grand nombre de cas de test pour garantir la fiabilité et la qualité de la calculatrice.






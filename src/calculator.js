const prompt = require('prompt-sync')();



class Calculator {

    n1;
    n2;
    oper;
    res;

    constructor() {

        this.n1 = prompt("Type first number : ");
        while (!this.n1.match('[0-9]')) {
            console.log("Enter a number !");
            this.n1 = prompt("Type first number : ");
        }
        this.oper = prompt("Type an operator: ");
        while (!this.oper.match('[+-/*%]')) {
            console.log("Try again");
            this.oper = prompt("Type an operator: ");
        }
        this.n2 = prompt("Type second number : ");
        while (!this.n2.match('[0-9]')) {
            console.log("Enter a number !");
            this.n2 = prompt("Type second number : ");
        }


        if (this.oper == "+") {
            this.res = Number(this.n1) + Number(this.n2);
        } else if (this.oper == "/") {
            this.res = Number(this.n1) / Number(this.n2);
        } else if (this.oper == "*") {
            this.res = Number(this.n1) * Number(this.n2);
        } else if (this.oper == "-") {
            this.res = Number(this.n1) - Number(this.n2);
        } else if (this.oper == "%") {
            this.res = (Number(this.n1) * 0.01) * Number(this.n2);
        }


    }

    getRes() {
        return this.res;
    }

    getOp() {
        return this.oper;
    }

    getSquare() {
        this.res*=this.res;
        return this.res;
    }

    getSqt() {
        return  Math.sqrt(this.res);
    }
}

let c1 = new Calculator();


module.exports = Calculator

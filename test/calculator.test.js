const expect = require('chai').expect;
const assert = require('assert');
const Calculator = require("../src/calculator");

describe('Testing the Calculator Functions : BDD Test ', function (){
    it('1. The Addition function', function(done) {
        let c1 = new Calculator();
        expect(c1.getOp()).to.equal("+");
        expect(c1.getRes()).to.equal(10);
        done();
    });
    it('2. The Subtraction function', function(done) {
        let c2 = new Calculator();
        expect(c2.getOp()).to.equal("-");
        expect(c2.getRes()).to.equal(15);
        done();
    });
    it('3. The Multiplication function', function(done) {
        let c3 = new Calculator();
        expect(c3.getOp()).to.equal("*");
        expect(c3.getRes()).to.equal(20);
        done();
    });
    it('4. The Division function', function(done) {
        let c4 = new Calculator();
        expect(c4.getOp()).to.equal("/");
        expect(c4.getRes()).to.equal(5);
        done();
    });
    it('5. The Percentage function', function(done) {
        let c5 = new Calculator();
        expect(c5.getOp()).to.equal("%");
        expect(c5.getRes()).to.equal(15);
        done();
    });
});

describe('Testing the Calculator Functions : TDD Test ', function () {
    it('1. Should return the Square of 2', function (done) {
        let c1 = new Calculator();
        assert.equal(4,c1.getSquare());
        done();
    });
    it('2. Should return the Square Root of 9', function (done) {
        let c2 = new Calculator();
        assert.equal(3,c2.getSqt());
        done();
    });
    
})

module.exports = Calculator;
